import { Component } from "react";

const Storygallery = () => {
  let story_data = [
    {
      src: "https://i.postimg.cc/TPh453Zz/upload.png",
      title: "Post Story",
      class: "story story1",
    },
    {
      src: "https://i.postimg.cc/XNPtfdVs/member-1.png",
      title: "Alison",
      class: "story story2",
    },
    {
      src: "https://i.postimg.cc/4NhqByys/member-2.png",
      title: "Jackson",
      class: "story story3",
    },
    {
      src: "https://i.postimg.cc/FH5qqvkc/member-3.png",
      title: "Samona",
      class: "story story4",
    },
    {
      src: "https://i.postimg.cc/Sx65bPcP/member-4.png",
      title: "John Doe",
      class: "story story5",
    },
  ];

  return (
    <div className="story-gallery">
      {story_data.map((data) => {
        return (
          <div className={data.class}>
            <img src={data.src} alt={data.title} />
            <p>{data.title}</p>
          </div>
        );
      })}
    </div>
  );
};

const Newpost = () => {
  return (
    <div className="write-post-container">
      <div className="user-profile">
        <img src="https://i.postimg.cc/cHg22LhR/profile-pic.png" alt="" />
        <div>
          <p>John Nicholson</p>
          <small>
            Public <i className="fas fa-caret-down"></i>
          </small>
        </div>
      </div>
      <div className="post-input-container">
        <textarea rows="3" placeholder="What's on your mind, John?"></textarea>
        <div className="add-post-links">
          <a href="/">
            <img src="https://i.postimg.cc/QMD2BDXs/live-video.png" alt="" />
            Live Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/6pKKZn0D/photo.png" alt="" />
            Photo/Video
          </a>
          <a href="/">
            <img src="https://i.postimg.cc/Pf6TBCdD/feeling.png" alt="" />
            Feling/Activity
          </a>
        </div>
      </div>
    </div>
  );
};

const Feedposts = () => {
  let feed_images = [
    { src: "https://i.postimg.cc/9fjhGTY6/feed-image-1.png" },
    { src: "https://i.postimg.cc/Xvc0xJ2p/feed-image-2.png" },
    { src: "https://i.postimg.cc/tJ7QXz9x/feed-image-3.png" },
    { src: "https://i.postimg.cc/hjDRYBwM/feed-image-4.png" },
    { src: "https://i.postimg.cc/ZRwztQzm/feed-image-5.png" },
  ];

  let activity_icons = [
    { src: "https://i.postimg.cc/pLKNXrMq/like-blue.png", num: "120" },
    { src: "https://i.postimg.cc/rmjMymWv/comments.png", num: "45" },
    { src: "https://i.postimg.cc/T2bBchpG/share.png", num: "20" },
  ];

  return (
    <>
      {feed_images.map((post) => {
        return (
          <div className="post-container" key={post.src}>
            <div className="post-row">
              <div className="user-profile">
                <img
                  src="https://i.postimg.cc/cHg22LhR/profile-pic.png"
                  alt=""
                />
                <div>
                  <p>John Nicholson</p>
                  <span>June 24 2021, 13:40 pm</span>
                </div>
              </div>
              <a href="/">
                <i className="fas fa-ellipsis-v"></i>
              </a>
            </div>
            <p className="post-text">
              Subscribe <span>@Vkive Tutorials</span> Youtube Channel to watch
              more videos on website development and UI desings.{" "}
              <a href="/">#VkiveTutorials</a> <a href="/">#YoutubeChannel</a>
            </p>
            <img src={post.src} className="post-img" alt="" />
            <div className="post-row">
              <div className="activity-icons">
                {activity_icons.map((data) => {
                  return (
                    <div>
                      <img src={data.src} alt="" />
                      {data.num}
                    </div>
                  );
                })}
              </div>
              <div className="post-porfile-icon">
                <img src={post.src} alt="" />
                <i className="fas fa-caret-down"></i>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

class MainComponent extends Component {
  render() {
    return (
      <div className="main-content">
        {/* Story Gallery component*/}
        <Storygallery />

        {/* new post component*/}
        <Newpost />

        {/* Feed posts component */}
        <Feedposts />

        <button type="button" className="load-more-btn">
          Load More
        </button>
      </div>
    );
  }
}

export default MainComponent;
