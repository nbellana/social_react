const RightSidebar = () => {
  const online_list = [
    {
      src: "https://i.postimg.cc/XNPtfdVs/member-1.png",
      name: "Alison Mina",
    },
    {
      src: "https://i.postimg.cc/4NhqByys/member-2.png",
      name: "Jackson Aston",
    },
    {
      src: "https://i.postimg.cc/FH5qqvkc/member-3.png",
      name: "Samona Rose",
    },
    {
      src: "https://i.postimg.cc/Sx65bPcP/member-4.png",
      name: "Mike Pérez",
    },
  ];

  const event = [
    {
      date: "18",
      month: "March",
      title: "Social Media",
    },
    {
      date: "22",
      month: "June",
      title: "Mobile Marketing",
    },
  ];

  return (
    <div className="right-sidebar">
      <div className="sidebar-title">
        <h4>Events</h4>
        <a href="/">See All</a>
      </div>

      {/* List of events */}
      {event.map((data) => {
        return (
          <div className="event">
            <div className="left-event">
              <h3>{data.date}</h3>
              <span>{data.month}</span>
            </div>
            <div className="right-event">
              <h4>{data.title}</h4>
              <p>
                <i className="fas fa-map-marker-alt"></i> Willson Tech Park
              </p>
              <a href="/">More Info</a>
            </div>
          </div>
        );
      })}

      {/* Advertisement section */}
      <div className="sidebar-title">
        <h4>Advertisement</h4>
        <a href="/">close</a>
      </div>
      <img
        src="https://i.postimg.cc/CLXYx9BL/advertisement.png"
        className="siderbar-ads"
        alt=""
      />

      {/* List of all online people */}
      <div className="sidebar-title">
        <h4>Conversation</h4>
        <a href="/">Hide Chat</a>
      </div>
      {online_list.map((data) => {
        return (
          <div className="online-list">
            <div className="online">
              <img src={data.src} alt="" />
            </div>
            <p>{data.name}</p>
          </div>
        );
      })}
    </div>
  );
};

export default RightSidebar;
